﻿using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;

// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable UseStringInterpolation


namespace Buddy
{
    /// <summary>
    /// Static class container for profile conditions dealing with Challenge Quests 
    /// </summary>
    public static class ChallengeConditions
    {
        /// <summary>
        /// Queries the game to see if a challenge is available for completion.  This only returns positive
        /// if your character is aware of the challenge and can complete it.  If it is know, but the challenge
        /// is on cooldown, it will return false.  If you've reached your maximum completion # (is there such a thing)
        /// the game should return false
        /// </summary>
        /// <param name="challengeId">The numeric ID of the Challenge</param>
        /// <returns>boolean value showing whether a challenge can be completed</returns>
        public static bool CanCompleteChallenge(int challengeId)
        {
            return GameManager.Lua.GetReturnValue<bool>(
                @"  local cid=" + challengeId + @"
                    local challenge 
                    for _,chall in pairs(ChallengesLib.GetActiveChallengeList()) do
                        if ( chall:GetId() == cid ) then
                            challenge = chall
                        end
                    end

                    if (challenge == nil) then
                        return false
                    end

                    if  not challenge: IsInCooldown() then
                        if not challenge: IsFullyComplete() then
                            return true
                        end
                    end
                    return false
            ");
        }
        
        /// <summary>
        /// Queries the game to determine the number of times you've completed a challenge. I use this to ensure that my
        /// leveling profile only does a challenge if it's your first time.  Relying solely on the 'CanCompleteChallenge'
        /// may cause problems once the first run is off cooldown.
        /// </summary>
        /// <param name="challengeId">The numeric ID of the Challenge</param>
        /// <returns>Integer, completion count</returns>
        public static int GetChallengeCompletionCount(int challengeId)
        {
            return GameManager.Lua.GetReturnValue<int>(
                @"  local cid=" + challengeId + @"
                    local challenge 
                    for _,chall in pairs(ChallengesLib.GetActiveChallengeList()) do
                        if ( chall:GetId() == cid ) then
                            challenge = chall
                        end
                    end

                    if (challenge == nil) then
                        return 0
                    end
                    return challenge:GetCompletionCount()
                ");
        }

        /// <summary>
        /// Queries the game to see if a challenge is on cooldown.  Useful for profiles that want to run
        /// many challenges and move between challenge areas trying to find one that can be performed.
        /// </summary>
        /// <param name="challengeId">The numeric ID of the Challenge</param>
        /// <returns>boolean showing if challenge is on cooldown</returns>
        public static bool IsChallengeOnCooldown(int challengeId)
        {
            return GameManager.Lua.GetReturnValue<bool>(
                @"  local cid=" + challengeId + @"
                    local challenge 
                    for _,chall in pairs(ChallengesLib.GetActiveChallengeList()) do
                        if ( chall:GetId() == cid ) then
                            challenge = chall
                        end
                    end

                    if (challenge == nil) then
                        return false
                    end
                    return challenge:IsInCooldown()
                ");
        }

        /// <summary>
        /// Queries the Challenge log to see if you actually know a challenge. I use this in profiles
        /// To determine if I need to perform an action to trigger a challenge. Like killing a mob, interacting
        /// with an item, or moving into a specific area.
        /// </summary>
        /// <param name="challengeId">The numeric ID of the Challenge</param>
        /// <returns>boolean value indicating whether a challenge is known to you</returns>
        public static bool IsChallengeKnown(int challengeId)
        {
            return GameManager.Lua.GetReturnValue<bool>(
                    @"  local cid=" + challengeId + @"
                    for _,chall in pairs(ChallengesLib.GetActiveChallengeList()) do
                        if ( chall:GetId() == cid ) then
                            return true
                        end
                    end
                    return false");
        }

        /// <summary>
        /// Queries the game to see whether or not a challenge is active.  An active state is clearly
        /// visible to a player by the challenge timer and objective display in the game, however
        /// during botting we need to know whether or not the challenge is actually active so we 
        /// can potentially activate it with the <ActivateChallenge/> Profile Tag
        /// </summary>
        /// <param name="challengeId">The numeric ID of the Challenge</param>
        /// <returns>boolean value indicating whether a challenge is actively running</returns>
        public static bool IsChallengeActive(int challengeId)
        {
            return GameManager.Lua.GetReturnValue<bool>(
                    @"  local cid=" + challengeId + @"
                    for _,chall in pairs(ChallengesLib.GetActiveChallengeList()) do
                        if ( chall:GetId() == cid ) then
                            return chall:IsActivated()
                        end
                    end
                    return false");
        }

        /// <summary>
        /// Run this command from the PEC to show the ID of the active challenge in the Wildbuddy Log.  This will not work if there
        /// isn't an active challenge running.
        /// </summary>
        public static void ShowActiveChallengeId()
        {
            var id = GameManager.Lua.GetReturnValue<int>(
                   @"for _,chall in pairs(ChallengesLib.GetActiveChallengeList()) do
                        if ( chall:IsActivated() ) then
                            return chall:GetId()
                        end
                    end
                    return 0
");
            ScriptProxy.Log(string.Format("Active Challenge ID: {0}",id));
        }
    }
}
