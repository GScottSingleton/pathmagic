﻿using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.Serialization;

// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable UseStringInterpolation

namespace Buddy.Wildstar.Engine.Profiles
{
    public abstract class BaseTagCreatures :BaseTag
    {
        
        [XmlAttribute("CreatureIds")] public string CreatureIds;

        public override bool Load(XElement e)
        {
            if (base.Load(e))
            {
                CreatureIds = e.LoadAttribute("CreatureIds", "");
                return true;
            }
            return false;
        }

        public override XElement Save()
        {
            var e = base.Save();
            e.SaveAttribute("CreatureIds", !string.IsNullOrEmpty(CreatureIds) ? string.Join(",", Creatures) : "");
            return e;
        }

        protected int[] Creatures
        {
            get
            {
                var creatures = new List<int>();
                foreach (var creature in CreatureIds.Split(','))
                {
                    int tmp;
                    if (int.TryParse(creature.Trim(), out tmp))
                        creatures.Add(tmp);
                }
                return creatures.ToArray();
            }
        }
    }
}
