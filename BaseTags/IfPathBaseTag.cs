﻿using System.Threading.Tasks;
using System.Xml.Serialization;
using Buddy.Coroutines;
using Buddy.Wildstar.Game;
// ReSharper disable UseStringInterpolation
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody

namespace Buddy.Wildstar.Engine.Profiles
{
    public abstract class IfPathBaseTag : BaseGroupTag
    {
       

        [XmlIgnore]
        protected abstract PlayerPathType PlayerPath { get; }

        public override async Task ProfileTagLogic()
        {
            //If not of PathType move on

            if (GameManager.LocalPlayer.PlayerPathType != PlayerPath)
            {
                IsTaskFinished = true;
                return;
            }


            //Check any Conditional Constraints

            if (CheckConstraints())
            {
                IsTaskFinished = true;
                return;
            }

            
            //If We're here then we passed conditions and must process

            foreach (var child in Children)
            {
                while (!child.IsFinished)
                    await child.ProfileTagLogic();
                child.Reset();
            }
            await Coroutine.Sleep(1);
            IsTaskFinished = true;
        }
    }
}
