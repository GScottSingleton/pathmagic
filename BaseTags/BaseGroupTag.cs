﻿using System;
using System.Xml.Linq;
using System.Xml.Serialization;
using Buddy.Wildstar.BotCommon;
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable UseStringInterpolation

namespace Buddy.Wildstar.Engine.Profiles
{
    public abstract class BaseGroupTag : ConditionalProfileGroupElement
    {
        [XmlAttribute("Debug")] public bool Debug;
        [XmlAttribute("QuestId")] public int QuestId = -1;
        [XmlAttribute("MissionId")] public int MissionId = -1;
        [XmlAttribute("QuestObjective")] public int QuestObjective = -1;

        [XmlIgnore] public override string Author { get { return PathMagic.PathMagic.Author; } }
        [XmlIgnore] public override Version Version { get { return PathMagic.PathMagic.Version; } }
        [XmlIgnore] public override bool IsFinished { get { return IsTaskFinished; } }

        protected bool IsTaskFinished;
        public override void Reset()
        {
            IsTaskFinished = false;
            base.Reset();
        }

        public override bool Load(XElement e)
        {
            if (base.Load(e))
            {
                QuestId = e.LoadAttribute("QuestId", -1);
                QuestObjective = e.LoadAttribute("QuestObjective", -1);
                Debug = e.LoadAttribute("Debug", false);
                MissionId = e.LoadAttribute("MissionId", -1);
                return true;
            }
            return false;
        }

        public override XElement Save()
        {
            var e = base.Save();
            e.SaveAttribute("QuestId", QuestId);
            e.SaveAttribute("QuestObjective", QuestObjective);
            e.SaveAttribute("MissionId", MissionId);
            return e;
        }

        [XmlIgnore]
        public bool HasMissionConstraint { get { return MissionId != -1; } }

        [XmlIgnore]
        public bool HasQuestConstraint { get { return QuestId != -1; } }

        [XmlIgnore]
        public bool HasQuestObjectiveContraint
        {
            get
            {
                return (QuestObjective != -1 && QuestId != -1);
            }
        }

        [XmlIgnore]
        protected bool MissionConstraintsMet
        {
            get
            {
                return (HasMissionConstraint && PathConditions.IsPathMissionComplete(MissionId));
            }
        }

        [XmlIgnore]
        protected bool QuestConstraintMet
        {
            get { return (HasQuestConstraint && ScriptProxy.IsQuestComplete(QuestId)); }
        }

        [XmlIgnore]
        protected bool QuestObjectiveConstraintMet
        {
            get
            {
                return (HasQuestObjectiveContraint && ScriptProxy.IsQuestObjectiveComplete(QuestId, QuestObjective));
            }
        }

        [XmlIgnore]
        protected bool HasCondition
        {
            get { return !string.IsNullOrEmpty(ConditionString); }
        }

        protected bool CheckConstraints()
        {
            if (QuestObjectiveConstraintMet)
            {
                ScriptProxy.Log("QuestObjectiveConstraint Met");
                return true;
            }
            if (QuestConstraintMet)
            {
                ScriptProxy.Log("Quest Constraint Met");
                return true;
            }
            if (MissionConstraintsMet)
            {
                ScriptProxy.Log("Mission Constraint Met");
                return true;
            }
            if (HasCondition && !Condition)
            {
                ScriptProxy.Log("Standard Condtion Constraint Met");
                return true;
            }
            return false;
        }
    }
}
