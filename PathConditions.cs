﻿using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;

// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable UseStringInterpolation

namespace Buddy
{
    public static class PathConditions
    {
        /// <summary>
        /// Queries the Path Episode Mission to determine completion state for the current character
        /// </summary>
        /// <param name="missionId">Numeric Mission ID</param>
        /// <returns>boolean indicating if mission has been completed</returns>
        public static bool IsPathMissionComplete(int missionId)
        {
            return GameManager.Lua.GetReturnValue<bool>(
               @"   local mid=" + missionId + @"
                    local isComplete = false
                    local pathEpisode = PlayerPathLib.GetPathEpisodeForZone()
                    if (pathEpisode) then
                        for key,mission in pairs(pathEpisode:GetMissions()) do
                            if(mission:GetId() == mid) then
                                return mission:IsComplete()
                            end
                        end
                    end
                    return isComplete
            ");
        }

        public static bool IsPathMissionComplete(int missionId, string zoneName)
        {
            return GameManager.Lua.GetReturnValue<bool>(
              @"   local mid=" + missionId + @"
                   local zone=""" + zoneName + @"""
                    local eps = PlayerPathLib.GetEpisodes()
                    for epKey,ep in pairs(eps) do
                        if(ep:GetWorldZone() == zone) then
                           for key,mission in pairs(ep:GetMissions()) do
                               if(mission:GetId() == mid) then
                                   return mission:IsComplete()
                               end
                           end
                        end
                    end
                    return false
            ");
        }

        public static string PathGetSummaryString(int missionId)
        {
            return GameManager.Lua.GetReturnValue<string>(
                          @"   local mid=" + missionId + @"
                    local isComplete = false
                    local pathEpisode = PlayerPathLib.GetPathEpisodeForZone()
                    if (pathEpisode) then
                        for key,mission in pairs(pathEpisode:GetMissions()) do
                            if(mission:GetId() == mid) then
                                return mission:GetSummary()
                            end
                        end
                    end
                    return isComplete
            ");
        }

        public static string PathGetUnlockString(int missionId)
        {
            return GameManager.Lua.GetReturnValue<string>(
                          @"   local mid=" + missionId + @"
                    local isComplete = false
                    local pathEpisode = PlayerPathLib.GetPathEpisodeForZone()
                    if (pathEpisode) then
                        for key,mission in pairs(pathEpisode:GetMissions()) do
                            if(mission:GetId() == mid) then
                                return mission:GetUnlockString()
                            end
                        end
                    end
                    return isComplete
            ");
        }

        public static int PathQuantityNeeded(int missionId)
        {
            return GameManager.Lua.GetReturnValue<int>(
                          @"   local mid=" + missionId + @"
                    local isComplete = false
                    local pathEpisode = PlayerPathLib.GetPathEpisodeForZone()
                    if (pathEpisode) then
                        for key,mission in pairs(pathEpisode:GetMissions()) do
                            if(mission:GetId() == mid) then
                                return mission:GetNumNeeded()
                            end
                        end
                    end
                    return isComplete
            ");
        }

        public static int PathQuantityCompleted(int missionId)
        {
            return GameManager.Lua.GetReturnValue<int>(
                          @"   local mid=" + missionId + @"
                    local isComplete = false
                    local pathEpisode = PlayerPathLib.GetPathEpisodeForZone()
                    if (pathEpisode) then
                        for key,mission in pairs(pathEpisode:GetMissions()) do
                            if(mission:GetId() == mid) then
                                return mission:GetNumCompleted()
                            end
                        end
                    end
                    return isComplete
            ");
        }

        /// <summary>
        /// Queries the Path Episode Mission to determine if the current mission is Active.  In the case
        /// of a soldier holdout or some other mission that can be shared between players it will return
        /// true if the mission is active even if you didn't start the mission.
        /// </summary>
        /// <param name="missionId">Numeric Mission ID</param>
        /// <returns></returns>
        public static bool IsPathMissionActive(int missionId)
        {
            return GameManager.Lua.GetReturnValue<bool>(
               @"   local mid=" + missionId + @"
                    local isComplete = false
                    local pathEpisode = PlayerPathLib.GetPathEpisodeForZone()
                    if (pathEpisode) then
                        for key,mission in pairs(pathEpisode:GetMissions()) do
                            if(mission:GetId() == mid) then
                                return mission:IsStarted()
                            end
                        end
                    end
                    return isComplete
            ");
        }

        /// <summary>
        /// Run from the PEC during a mission to dump the ID of the Path Mission
        /// </summary>
        public static void ShowActivePathMission()
        {
            var id = GameManager.Lua.GetReturnValue<int>(@"
                    local peps = PlayerPathLib.GetPathEpisodeForZone()
                    if peps then
                        for key,mission in pairs(peps:GetMissions()) do
                            if(mission:IsStarted()) then
                                return mission:GetId()
                            end
                        end
                    end
                    return 0
            ");
            ScriptProxy.Log(string.Format("Active Path Mission ID: {0}",id));
        }

        public static void ShowAllPathMissions()
        {
            var missions = GameManager.Lua.GetReturnValue<string>(
                @"
                local peps = PlayerPathLib.GetPathEpisodeForZone()
                local returnString = """"
                if peps then
                    for key,mission in pairs(peps:GetMissions()) do
                        returnString = returnString..tostring(mission:GetId()).."",""..mission:GetName().."",""..tostring(mission:IsStarted()).."",""..tostring(mission:IsComplete())..""\n""
                    end
                    return returnString
                end
                ");

            ScriptProxy.Log("\n********** Path Mission Dump ***************");
            ScriptProxy.Log(missions);
        }
        
        public static void ShowAllInventory()
        {
            foreach (var item in GameManager.Inventory.Tradeskill.Materials)
            {
                ScriptProxy.Log(string.Format("[{0}] {1} [Qty {2}]",item.Id,item.ItemRecord.Name,item.Count));
            }
        }

        public static int GetTradeMaterialCount(int itemId)
        {
            return GameManager.Inventory.Tradeskill.GetMaterialCount(itemId);
        }

    }
}
