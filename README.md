# Path Magic #

 - Path, Challenge, and other sundries for Wildbuddy questing profiles.

### Summary ###

* Challenge related profile Tags and conditions
* Path Mission related profile Tags and conditions
* Other Misc. profile Tags that make my life easier
* [Visit Discussion thread on Wildbuddy Forums](https://www.thebuddyforum.com/wildbuddy-forum/wildbuddy-profiles/230316-pathmagic-path-challenge-related-profile-tags-conditional-statements.html)
* [Visit Path Magic Issue Tracker](https://bitbucket.org/GScottSingleton/pathmagic/issues?status=new&status=open)
* [Visit Path Magic WIKI - Work In Progress](https://bitbucket.org/GScottSingleton/pathmagic/wiki/Home)

### Introduction ###

Tired of having your profiles just grind XP?  Now you can perform Path Mission and Challenge activities in your profiles.


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact