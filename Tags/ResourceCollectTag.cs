﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using Buddy.Common.Math;
using Buddy.Coroutines;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.BotCommon.ProfileTags.Quest;
using Buddy.Wildstar.Game;
// ReSharper disable NotResolvedInText
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable UseStringInterpolation

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("ResourceCollect")]
    [XmlRoot("ResourceCollect")]
    public class ResourceCollectTag : CollectTag
    {
        [XmlIgnore]
        public override string Name { get { return "ResourceCollect"; } }
        [XmlIgnore]
        public override string Author { get { return PathMagic.PathMagic.Author; } }
        [XmlIgnore]
        public override Version Version { get { return PathMagic.PathMagic.Version; } }

        [XmlIgnore]
        public override bool IsFinished { get { return _isFinished; } }

        private bool _isFinished;
        public override void Reset()
        {
            _isFinished = false;
            base.Reset();
        }

        [XmlAttribute("ItemId")]
        public int ItemId = -1;
        [XmlAttribute("Quantity")]
        public int Quantity = -1;

        [XmlAttribute("Creature")]
        public int CreatureId = -1;
        [XmlAttribute("X")]
        public float X = -1f;
        [XmlAttribute("Y")]
        public float Y = -1f;
        [XmlAttribute("Z")]
        public float Z = -1f;
        [XmlAttribute("Radius")]
        public float Radius = 200f;

        public override XElement Save()
        {
            var e = base.Save();
            e.SaveAttribute("ItemId", ItemId);
            e.SaveAttribute("Quantity", Quantity);
            e.SaveAttribute("Creature", CreatureId);
            e.SaveAttribute("X", X);
            e.SaveAttribute("Y", Y);
            e.SaveAttribute("Z", Z);
            e.SaveAttribute("Radius", Radius);
            return e;
        }

        public override bool Load(XElement e)
        {
            if (e.Name != ElementName) return false;
            ItemId = e.LoadAttribute("ItemId", -1);
            Quantity = e.LoadAttribute("Quantity", Quantity);
            CreatureId = e.LoadAttribute("Creature", -1);
            X = e.LoadAttribute("X", -1f);
            Y = e.LoadAttribute("Y", -1f);
            Z = e.LoadAttribute("Z", -1f);
            Radius = e.LoadAttribute("Radius", 200f);

            return base.Load(e);
        }


        public override async Task ProfileTagLogic()
        {
            var home = new Vector3(X, Y, Z);
            while (true)
            {
                if (ItemId == -1) throw new ArgumentException("ItemId is Not Specified.  It is required", "ItemId");
                if (Quantity == -1) throw new ArgumentException("Quantity is Not Specified. It is Required", "Quantity");
                if (CreatureId == -1) throw new ArgumentException("CreatureId is not Specified. It is Required", "CreatureId");

                if (GameManager.Inventory.Tradeskill.GetMaterialCount(ItemId) >= Quantity) break;

                //Look for objects within radius of _home
                var resource = GameManager.Actors.Values.Where(a => a.Position.Distance(home) <= Radius && a.CreatureId == CreatureId).OrderBy(a => a.Distance).FirstOrDefault();
                if (resource == null)
                {
                    //Go Back to start to rescan
                    await CommonBehaviors.MoveWithin(home, InteractRange, true, true);
                }
                else if (resource.Distance > InteractRange)
                {
                    await CommonBehaviors.MoveWithin(resource.Position, InteractRange, true, true);
                }
                else
                {
                    //Let's do this!
                    resource.Target();
                    resource.Interact();
                    await Coroutine.Sleep(4000);
                }
                await Coroutine.Sleep(1);
            }
            _isFinished = true;
        }
    }
}
