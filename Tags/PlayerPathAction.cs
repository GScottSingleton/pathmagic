﻿using Buddy.Coroutines;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;
using Buddy.Wildstar.Game.Actors;
using System.Threading.Tasks;
using System.Xml.Serialization;

// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("PlayerPathAction")]
    [XmlRoot("PlayerPathAction")]
    public class PlayerPathActionTag : BaseTag
    {

        [XmlIgnore] public override string Name { get { return "PlayerPathAction"; } }

        private Actor _actor;
        private Coroutine _behavior;
       
        public override async Task ProfileTagLogic()
        {
            while (true)
            {
                const int completionRange = 1;
                if (GameManager.LocalPlayer.Position.Distance2D(HomeLocation) < completionRange)
                {
                    await PathAction();
                    await Coroutine.Sleep(2000);
                    break;
                }
                await CommonBehaviors.MoveWithin(HomeLocation, completionRange, true, true);
                await Coroutine.Sleep(1);
            }
            IsTaskFinished = true;
        }

        [XmlIgnore]
        public bool IsScanBotActive
        {
            get { return GameManager.Lua.GetReturnValue<bool>(@"return PlayerPathLib.ScientistHasScanBot()"); }
        }

        private async Task PathAction()
        {
            GameManager.Lua.DoString("PlayerPathLib.PathAction()");
            await Coroutine.Sleep(2000);
        }
    }
}
