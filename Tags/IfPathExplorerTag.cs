﻿using System.Xml.Serialization;
using Buddy.Wildstar.Game;

// ReSharper disable UseStringInterpolation
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("IfPathExplorer")]
    [XmlRoot("IfPathExplorer")]
    public class IfPathExplorerTag : IfPathBaseTag
    {
        public override string Name { get { return "IfPathExplorer"; } }
        protected override PlayerPathType PlayerPath { get {return PlayerPathType.Explorer;} }
    }
}
