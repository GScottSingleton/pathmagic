﻿using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using Buddy.Common.Math;
using Buddy.Coroutines;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;
using Buddy.Wildstar.Game.Actors;

// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable UseStringInterpolation

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("PathActionOn")]
    [XmlRoot("PathActionOn")]
    public class PathActionOnTag : BaseTagCreatures
    {
        [XmlIgnore]
        public override string Name { get { return "PathActionOn"; } }

        [XmlAttribute("CastRange")] public float CastRange = 10f;
        [XmlAttribute("InteractFirst")] public bool InteractFirst;
        
        
        private Actor _actor;

        public override bool Load(XElement e)
        {
            CastRange = e.LoadAttribute("CastRange", 10f);
            InteractFirst = e.LoadAttribute("InteractFirst", false);
            return base.Load(e);
        }

        public override XElement Save()
        {
            var e = base.Save();
            e.SaveAttribute("CastRange", CastRange);
            e.SaveAttribute("InteractFirst", InteractFirst);
            return e;
        }


        public override async Task ProfileTagLogic()
        {
            
            while (true)
            {
                if (CheckConstraints()) break;
                if (!HasLocation) break;

                _actor = GameManager.Actors.Values.Where( 
                    b => Vector3.Distance(HomeLocation, b.Position) <= Radius 
                    && Creatures.Contains(b.CreatureId)
                    && !b.IsBusy
                    && !b.IsDead
                    && b.IsAlive
                    ).OrderBy(c=>c.Distance).FirstOrDefault();

                if (_actor == null || !_actor.IsValid || _actor.IsDead)
                    await CommonBehaviors.MoveWithin(HomeLocation, 1, true, true);
                else if (_actor.Distance < CastRange)
                {
                    //Target, and don't return until target confirmed
                    await Coroutine.Wait(500, () =>
                                              {
                                                  _actor.Target();
                                                  return GameManager.LocalPlayer.CurrentTarget == _actor;
                                              } );
                    if (GameManager.LocalPlayer.CurrentTarget == _actor)
                    {
                        if (InteractFirst && _actor.CanInteract)
                        {
                            _actor.Interact();
                            await Coroutine.Wait(2000, () => GameManager.LocalPlayer.IsCasting);
                        }
                        //cHASE
                        await CommonBehaviors.MoveWithin(_actor.Position, CastRange, true, true);
                        await PathAction();
                    }
                    await Coroutine.Wait(2000, () => GameManager.LocalPlayer.IsCasting);
                }
                else
                {
                    await CommonBehaviors.MoveWithin(_actor.Position, CastRange, true, true);
                }
                await Coroutine.Sleep(1);
            }
            IsTaskFinished = true;
        }

        private async Task PathAction()
        {
            GameManager.Lua.DoString("PlayerPathLib.PathAction()");
            await Coroutine.Sleep(500);
        }
        
    }
}
