﻿using Buddy.Coroutines;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;
using Buddy.Wildstar.Game.Actors;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable UseStringInterpolation

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("ScienceScanArea")]
    [XmlRoot("ScienceScanArea")]
    public class ScienceScanAreaTag : BaseTag
    {
        [XmlIgnore]
        public override string Name { get { return "ScienceScanArea"; } }
        
        private Actor _actor;
        private Coroutine _behavior;


        public override async Task ProfileTagLogic()
        {
            if (!IsScanBotActive)
                await ActivateScanBot();

            while (true)
            {
                if (HasMissionConstraint && PathConditions.IsPathMissionComplete(MissionId)) break;
                
                if (GameManager.LocalPlayer.Position.Distance2D(HomeLocation) < InteractRange)
                {
                    //We're inside of radius; Scan for targets

                    _actor = GameManager.Actors.Values.Where(
                        a => !a.IsBusy
                             && a.ActivationTypes.Contains(ActivationType.ScientistScannable)
                             && a.Distance <= Radius).OrderBy(a => a.Distance).FirstOrDefault();

                    if (_actor == null)
                    {
                        //Nothing Scannable, move back to origin
                        await CommonBehaviors.MoveWithin(HomeLocation, InteractRange, true, true);
                    }
                    else if (_actor.Distance > InteractRange)
                    {
                        await CommonBehaviors.MoveWithin(_actor.Position, InteractRange, true, true);
                    }
                    else
                    {
                        await ScanTarget(_actor);
                    }
                }
                await CommonBehaviors.MoveWithin(HomeLocation, InteractRange, true, true);
                await Coroutine.Sleep(1);
            }
            IsTaskFinished = true;
        }

        [XmlIgnore]
        public bool IsScanBotActive
        {
            get { return GameManager.Lua.GetReturnValue<bool>(@"return PlayerPathLib.ScientistHasScanBot()"); }
        }

        private async Task ActivateScanBot()
        {
            GameManager.Lua.DoString(
                    @"if not PlayerPathLib.ScientistHasScanBot() then
                            PlayerPathLib.ScientistToggleScanBot()
                       end");
            await Coroutine.Sleep(2000);
        }

        private async Task ScanTarget(Actor actor)
        {
            actor.Target();
            GameManager.Lua.DoString("PlayerPathLib.PathAction()");
            await Coroutine.Sleep(5000);
        }
    }
}
