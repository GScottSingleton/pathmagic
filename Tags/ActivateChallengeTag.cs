﻿using Buddy.Common.Math;
using Buddy.Coroutines;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable UseStringInterpolation

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("ActivateChallenge")]
    [XmlRoot("ActivateChallenge")]
    public class ActivateChallengeTag : BaseTag
    {

        [XmlIgnore] public override string Name { get { return "ActivateChallenge"; } }
        [XmlAttribute("ChallengeId")] public int ChallengeId = -1;

        public override XElement Save()
        {
            var e = base.Save();
            e.SaveAttribute("ChallengeId", ChallengeId);
            return e;
        }

        public override bool Load(XElement e)
        {
            if (base.Load(e))
            {
                ChallengeId = e.LoadAttribute("ChallengeId", -1);
                return true;
            }
            return false;
        }

        public override async Task ProfileTagLogic()
        {
            while (true)
            {
                if (ChallengeId == -1) break;
                if (ChallengeConditions.IsChallengeActive(ChallengeId)) break;

                var destination = new Vector3(X, Y, Z);
                const int completionRange = 10;
                if (GameManager.LocalPlayer.Position.Distance2D(destination) < completionRange)
                {
                    await ActivateChallenge();
                    break;
                }
                await CommonBehaviors.MoveWithin(destination, completionRange, true, true);
                await Coroutine.Sleep(1);
            }
            IsTaskFinished = true;
        }

        private async Task ActivateChallenge()
        {
            GameManager.Lua.DoString(@"ChallengesLib.ActivateChallenge(" + ChallengeId + @")");
            await Coroutine.Sleep(500);
        }
    }

}
