﻿using System.Xml.Serialization;
using Buddy.Wildstar.Game;

// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("IfPathSoldier")]
    [XmlRoot("IfPathSoldier")]
    public class IfPathSoldierTag : IfPathBaseTag
    {
        public override string Name { get { return "IfPathSoldier"; } }
        protected override PlayerPathType PlayerPath { get { return PlayerPathType.Soldier; } }
    }
}
