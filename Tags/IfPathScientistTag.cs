﻿using System.Xml.Serialization;
using Buddy.Wildstar.Game;

// ReSharper disable UseStringInterpolation
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("IfPathScientist")]
    [XmlRoot("IfPathScientist")]
    public class IfPathScientistTag : IfPathBaseTag
    {
        public override string Name { get { return "IfPathScientist"; } }
        protected override PlayerPathType PlayerPath { get {return PlayerPathType.Scientist;} }
    }
}
