﻿using System;
using Buddy.Common.Math;
using Buddy.Coroutines;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;
using Buddy.Wildstar.Game.Actors;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable UseStringInterpolation

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("DefendArea")]
    [XmlRoot("DefendArea")]
    public class DefendArea : ConditionalProfileElement
    {

        #region ProfileElement Implementation

        [XmlIgnore] public override string Name { get { return "DefendArea"; } }
        [XmlIgnore] public override string Author { get { return PathMagic.PathMagic.Author; } }
        [XmlIgnore] public override Version Version { get { return PathMagic.PathMagic.Version; } }

        private bool _isFinished;
        [XmlIgnore]
        public override bool IsFinished { get { return _isFinished; } }

        public override void Reset()
        {
            _isFinished = false;
            base.Reset();
        }

        #endregion

        #region QuestProfileElement Implementation

        [XmlIgnore]
        public bool HasObjectiveConstraint
        {
            get
            {
                return QuestObjective != -1;
            }
        }

        [XmlIgnore]
        public bool IsObjectiveComplete
        {
            get
            {
                return ScriptProxy.IsQuestObjectiveComplete(QuestId, QuestObjective);
            }
        }

        [XmlIgnore]
        public bool HasQuestConstraint
        {
            get
            {
                return QuestId != -1;
            }
        }

        [XmlIgnore]
        public bool IsQuestAccepted
        {
            get
            {
                return GameManager.Lua.GetReturnValue<bool>(
                  @"for _,category in pairs(QuestLib.GetKnownCategories()) do
                        for _,episode in pairs(category:GetEpisodes()) do
                            for _,quest in pairs(episode:GetAllQuests(category:GetId())) do
                                if(quest:GetId() == " + QuestId + @") then
                                    if(quest:GetState() == Quest.QuestState_Accepted) then
                                        return true
                                    end
                                end
                            end
                        end
                    end
                    return false
                ");
            }
        }

        [XmlIgnore]
        public bool HasMissionConstraint
        {
            get { return (MissionId != -1); }
        }

        #endregion

        [XmlAttribute("QuestId")] public int QuestId = -1;
        [XmlAttribute("QuestObjective")] public int QuestObjective = -1;
        [XmlAttribute("CreatureIds")] public string Creature = "";
        [XmlAttribute("MissionId")] public int MissionId = -1;
        [XmlAttribute("X")] public float X = -1;
        [XmlAttribute("Y")] public float Y = -1;
        [XmlAttribute("Z")] public float Z = -1;
        [XmlAttribute("Radius")] public float Radius = 10;
        [XmlAttribute("PullRange")] public float PullRange = 10;

        private int[] Creatures
        {
            get
            {
                var creatures = new List<int>();

                foreach (var creature in Creature.Split(','))
                {
                    int tmp;
                    if (int.TryParse(creature.Trim(), out tmp))
                        creatures.Add(tmp);
                }
                return creatures.ToArray();
            }
        }

        private Actor _actor;
        private Coroutine _behavior;
        private Vector3 _home;

        public override async Task ProfileTagLogic()
        {
            _home = new Vector3(X,Y,Z);

            while (true)
            {
                if (HasMissionConstraint && PathConditions.IsPathMissionComplete(MissionId))
                {
                    ScriptProxy.Log("Mission Constraint Match");
                    break;
                }
                if (HasCondition && !Condition)
                {
                    ScriptProxy.Log("Regular Condition Match");
                    break;
                }
                if (HasQuestConstraint && !IsQuestAccepted)
                {
                    ScriptProxy.Log("Quest Constraint Match");
                    break;
                }
                if (HasQuestConstraint && HasObjectiveConstraint && IsObjectiveComplete)
                {
                    ScriptProxy.Log("Quest Objective Constraint Match");
                    break;
                }

                if (!GameManager.LocalPlayer.IsInCombat && GameManager.LocalPlayer.CurrentTarget != null && !GameManager.LocalPlayer.CurrentTarget.IsValid)
                {
                    ScriptProxy.Log("In Combat and CurrentTarget != Null");
                    await Coroutine.Sleep(500);
                }
                else
                {
                    // Find killable mob in defense zone
                    ScriptProxy.Log("Scanning for hostiles in radius of: " +
                                    Radius.ToString(CultureInfo.InvariantCulture));
                    var hostile =
                        GameManager.Actors.Values.Where(
                            b => Vector3.Distance(_home, b.Position) <= Radius && b.Disposition == Disposition.Hostile)
                            .OrderBy(c => Vector3.Distance(_home, c.Position))
                            .FirstOrDefault();

                    if (hostile == null || !hostile.IsValid)
                    {
                        await CommonBehaviors.MoveWithin(_home, 1, true, true);
                        await Coroutine.Sleep(500);
                        continue;
                    }
                        
                    if (hostile.Distance < PullRange + 5)
                    {
                        hostile.Target();
                        await Coroutine.Wait(500, () => GameManager.LocalPlayer.CurrentTarget != null);
                        await GameEngine.CurrentRoutine.Pull(hostile);
                        await Coroutine.Sleep(500);
                        continue;
                    }
                    await CommonBehaviors.MoveWithin(hostile.Position, PullRange, true, true);
                    await Coroutine.Sleep(500);
                }
            }
            _isFinished = true;
        }

        private bool ValidTarget(Actor actor)
        {
            if (actor == null)
                return false;

            if (!actor.IsValid)
                return false;

            if (actor.ActorType == ActorType.Player)
                return false;

            if (actor.ActorType == ActorType.Pet)
                return false;

            if (actor.ActorType == ActorType.Harvest)
                return false;

            if (!actor.IsAlive || actor.IsDead)
            {
                ScriptProxy.Log(string.Format("[{0}] : Is Dead", actor.Name));
                return false;
            }


            if (actor.RankRaw == 1)
            {
                ScriptProxy.Log(string.Format("[{0}] : RankRaw 1",actor.Name));
                return false;
            }

            if (actor.RankRaw >= 6)
            {
                ScriptProxy.Log(string.Format("[{0}] : RankRaw 2", actor.Name));
                return false;
            }

            if (actor.Disposition == Disposition.Friendly)
            {
                ScriptProxy.Log(string.Format("[{0}] : Friendly", actor.Name));
                return false;
            }

            if (actor.Disposition == Disposition.Unknown)
            {
                ScriptProxy.Log(string.Format("[{0}] : Disposition unknown", actor.Name));
                return false;
            }

            if ((actor.MovementFlags & MoveFlags.InAir) != 0)
            {
                ScriptProxy.Log(string.Format("[{0}] : In Air", actor.Name));
                return false;
            }


            if (!actor.CreatureInfo.IsValid)
            {
                ScriptProxy.Log(string.Format("[{0}] : CreatureInfo Invalid", actor.Name));
                return false;
            }


            if (!actor.CreatureInfo.CanBeAttacked)
            {
                ScriptProxy.Log(string.Format("[{0}] : Can't Be Attacked", actor.Name));
                return false;
            }


            if (actor.Position.Distance(new Vector3(X, Y, Z)) > Radius)
            {
                ScriptProxy.Log(string.Format("[{0}] : OUtside of home range: {1}", actor.Name,actor.Position.Distance(new Vector3(X,Y,Z))));
                return false;
            }
                
            ScriptProxy.Log(string.Format("GOOD ACTOR: {0}",actor.Name));

            return true;
        }
    }
}
