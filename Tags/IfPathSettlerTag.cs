﻿using System.Xml.Serialization;
using Buddy.Wildstar.Game;

// ReSharper disable UseStringInterpolation
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("IfPathSettler")]
    [XmlRoot("IfPathSettler")]
    public class IfPathSettlerTag : IfPathBaseTag
    {
        public override string Name { get { return "IfPathSettler"; } }
        protected override PlayerPathType PlayerPath { get {return PlayerPathType.Settler;} }
    }
}
