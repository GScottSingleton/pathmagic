﻿using Buddy.Coroutines;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;
using Buddy.Wildstar.Game.Actors;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("ScienceScanSingle")]
    [XmlRoot("ScienceScanSingle")]
    public class ScienceScanSingleTag : BaseTagCreatures
    {

        [XmlIgnore] public override string Name {get { return "ScienceScanSingle"; } }



        private Actor _actor;
        private Coroutine _behavior;
        
        public override async Task ProfileTagLogic()
        {
            while (true)
            {
                const int completionRange = 2;
                if (GameManager.LocalPlayer.Position.Distance2D(HomeLocation) < completionRange)
                {
                    _actor = GameManager.Actors.Values.Where(a => Creatures.Contains(a.CreatureId) && a.ActivationTypes.Contains(ActivationType.ScientistScannable)).OrderBy(a => a.Distance).FirstOrDefault();
                    if (_actor != null)
                    {
                        if (Debug)
                        {
                            ScriptProxy.Log("Looking at Actor: " + _actor.Name);
                            ScriptProxy.Log("Status of Scan Bot: " + IsScanBotActive);
                        }
                        await ScanTarget(_actor);
                        break;
                    }
                    ScriptProxy.Log("At Destination; Moving On.");
                    break;
                }
                await CommonBehaviors.MoveWithin(HomeLocation, completionRange, true, true);
                await Coroutine.Sleep(1);
            }
            IsTaskFinished = true;
        }

        [XmlIgnore]
        public bool IsScanBotActive
        {
             get { return GameManager.Lua.GetReturnValue<bool>(@"return PlayerPathLib.ScientistHasScanBot()"); }
        }


        private async Task ActivateScanBot()
        {
            GameManager.Lua.DoString(
                    @"if not PlayerPathLib.ScientistHasScanBot() then
                            PlayerPathLib.ScientistToggleScanBot()
                       end");
            await Coroutine.Sleep(2000);
        }

        private async Task ScanTarget(Actor actor)
        {
            actor.Target();
            GameManager.Lua.DoString("PlayerPathLib.PathAction()");
            await Coroutine.Sleep(2000);
        }


    }
}
