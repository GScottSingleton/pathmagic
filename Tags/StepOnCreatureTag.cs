﻿using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Buddy.Coroutines;
using Buddy.Wildstar.BotCommon;
using Buddy.Wildstar.Game;
using Buddy.Wildstar.Game.Actors;

// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable UseStringInterpolation

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("StepOnCreature")]
    [XmlRoot("StepOnCreature")]
    public class StepOnCreatureTag : BaseTagCreatures
    {

        [XmlIgnore] public override string Name { get { return "StepOnCreature"; } }

        private Actor _actor;
        private Coroutine _behavior;
        private Blacklist _blacklist;

     
        public override async Task ProfileTagLogic()
        {
            while (true)
            {
                if (CheckConstraints()) break;
                _actor = GameManager.Actors.Values.Where(a => Creatures.Contains(a.CreatureId) && !a.IsBusy && a.Position.Distance(HomeLocation) <= Radius).OrderBy(a => a.Distance).FirstOrDefault();
                
                // Can't find anything? Move back to given point.
                if (_actor == null || !_actor.IsValid)
                    await CommonBehaviors.MoveWithin(HomeLocation, 1, true, true);
                else
                {
                    // Have something, let's try to walk right onto it
                    const float stepRange = 0.1F;
                    await CommonBehaviors.MoveWithin(_actor.Position, stepRange, true, true);
                }
                await Coroutine.Sleep(1);
            }
            IsTaskFinished = true;
        }

    }
}
