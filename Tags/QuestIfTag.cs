﻿using System.Threading.Tasks;
using System.Xml.Serialization;

// ReSharper disable UseStringInterpolation
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody

namespace Buddy.Wildstar.Engine.Profiles
{
    [ProfileElementName("QuestIf")]
    [XmlRoot("QuestIf")]
    public class QuestIfTag : BaseGroupTag
    {
        public override string Name { get { return "QuestIf"; } }

        public override async Task ProfileTagLogic()
        {
            if (QuestId != -1)
            {
                if (CheckConstraints())
                {
                    IsTaskFinished = true;
                    return;
                }

                foreach (var child in Children)
                {
                    while (!child.IsFinished)
                        await child.ProfileTagLogic();
                    child.Reset();
                }
                IsTaskFinished = true;
            }
        }
    }
}
