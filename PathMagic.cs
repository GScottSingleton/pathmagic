﻿using System;

// ReSharper disable ConvertPropertyToExpressionBody

namespace PathMagic
{
    public static class PathMagic
    {
        public static Version Version
        {
            get
            {
                return new Version(0,2,0);
            }
        }

        public static string Author
        {
            get { return "Brewer"; }
        }
    }
}
